#
# auto delete resource on push 
# don't forget about the output.tf file
/*  */
resource "vsphere_virtual_machine" "demo-vm" {
  name             = var.vm_name
  resource_pool_id = data.vsphere_resource_pool.tf.id
  folder           = data.vsphere_folder.folder.path
  datastore_id     = data.vsphere_datastore.lab1_datastore.id
  host_system_id   = data.vsphere_host.host3.id
  datacenter_id    = data.vsphere_datacenter.rabat_dc.id
  guest_id         = "other3xLinux64Guest"
  #guest_id = data.vsphere_virtual_machine.template.guest_id
  #  scsi_type = data.vsphere_virtual_machine.template.scsi_type

  num_cpus = var.vm_cpu
  memory   = var.vm_ram
  disk {
    label = "disk0"
    size  = var.vm_disk
  }
  
  network_interface {
    network_id = data.vsphere_network.network.id
  }
  cdrom {
    client_device = true
  }
  
  #clone {
  #  template_uuid = data.vsphere_virtual_machine.template.id
  #}

  ovf_deploy {
    allow_unverified_ssl_cert = false
    remote_ovf_url            = var.remote_ovf_url
    #"http://nas.company-name.com/ubuntu20.04.ova"
    disk_provisioning         = "thin"
    ovf_network_map = {
      "Network 1" = data.vsphere_network.network.id
    }
  }
  
  vapp {
    properties = {
      user-data = base64encode(file("../../cloud-init.yml"))
    }
  }

}

 
#
