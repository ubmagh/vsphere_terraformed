variable vsphere_user {
  type        = string
  description = "VSphere User"
}

variable vsphere_password {
  type        = string
  description = "VSphere Password"
}

variable vsphere_server {
  type        = string
  default     = "vcsa.maroc.company-name.com"
  description = "VSphere Server"
}

variable remote_ovf_url {
  type        = string
  description = "remote_ovf_url"
}

variable vm_name {
  type        = string
  description = "VM name"
}

variable vm_cpu {
  type        = number
  description = "CPU"
}

variable vm_ram {
  type        = number
  description = "RAM"
}

variable vm_disk {
  type        = number
  description = "DISK"
}
