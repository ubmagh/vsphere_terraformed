
data "vsphere_datacenter" "rabat_dc" {
  name = "Lab"
}

data "vsphere_datastore" "lab1_datastore" {
  name          = "DS-LAB1"
  datacenter_id = data.vsphere_datacenter.rabat_dc.id
}

data "vsphere_resource_pool" "tf" {
  name          = "tf"
  datacenter_id = data.vsphere_datacenter.rabat_dc.id
}

data "vsphere_folder" "folder" {
  path = "/${data.vsphere_datacenter.rabat_dc.name}/vm/lab"
}



data "vsphere_host" "host3" {
  name          = "10.42.2.26"
  datacenter_id = data.vsphere_datacenter.rabat_dc.id
}

data "vsphere_network" "network" {
  name          = "VM Network"
  datacenter_id = data.vsphere_datacenter.rabat_dc.id
}

#data "vsphere_virtual_machine" "template" {
#  name          = "ubuntu20.04"
#  datacenter_id = data.vsphere_datacenter.rabat_dc.id
#}

