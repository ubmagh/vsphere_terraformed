# Docker image used for this GitLab pipeline

# docker build -t scoma/terraform_ubuntu:v3 .

FROM harbor-devops.company-name.com/dockerhub-proxy/library/ubuntu:jammy 


RUN apt update
RUN apt upgrade -y
RUN apt install ccrypt gnupg software-properties-common git wget -y
RUN useradd -m terraform-user
RUN  wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

# RUN gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint

RUN  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" |  tee /etc/apt/sources.list.d/hashicorp.list

RUN apt update

RUN apt install terraform=1.2.0 -y
USER terraform-user
CMD ["/bin/bash"]

# change 16