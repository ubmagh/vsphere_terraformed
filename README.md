# VSphere VMs provisioning with Terraform

> The pipeline needs a docker runner/executor !

In this repo, we tried applying GitOps approach to provision VMs on VSphere. However, the approach is not fully satisfied especially if we wanna do a destroy operation.


## how  to create new VM : (let's say vm1 for example)

* create a directory `vm1` inside `environments` directory, and put inside the ccrypted tfvars. recommended to keep same name for the vm (vm1)

* Create a pipeline yaml file inside `pipelines` let's say `vm1.gitlab-ci.yml`. Inside of it extend the base pipeline config `.base.gitlab-ci.yml`, get inspired from existing custome pipelines files to create your own, you should notice using the VM name in all jobs names & passed variables .(it's easy)

* Last step, got to `.gitlab-ci.yml`, add new stage with the name of your vm (- VM1). And at the end of the file, you should importe the file you created `vm1.gitlab-ci.yml`, in the include block.

* run the pipeline, and then you can see that `init` and `plan` steps/jobs are executed automatically, in contrast of `apply` & `destroy`


> For `apply` & `destroy` commands/stages, you may need to execute `init` in first place as the cache might get cleared.

> You might need to clear cache of the pipeline manually time to time, i think it could cause problems 


## TOdos :

- [ ] externalize SSH public keys (VAULT ?)


